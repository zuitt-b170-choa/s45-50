// Base Imports
import React, {useState} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

//App Components
import AppNavbar from './components/AppNavbar.js';

// app imports
import userContext from './userContext.jsx';

//Page Components
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Notfound from './pages/NotFound.jsx';


export default function App(){
	// localStorage.getItem - used to retrieve a piece or the whole set of information inside the localstorage the code below detects if there is a user logged in through the use of localStorage.setItem in the login.js
	const [user,setUser]=useState({access: localStorage.getItem('access')});

	const unsetUser = ()=>{
		localStorage.clear();
		setUser({access: null});
	}

	/*
	path ="*" all othger unspecified routes. this is to make sure that all other routes, beside the ones in the return statement will render the Error page.
	*/
	/*
		The provider component inside userCOntext is what allows other components to consume or use the context. Any component which is not wrapped by the Provider will have access to the values provided in the context
	*/
	return(
		<userContext.Provider value={{user, setUser, unsetUser}}>
		<Router>
	      <AppNavbar/>
	        <Routes>
	          <Route path = "/" element= {<Home />}/>
	          <Route path = "/courses" element={<Courses />}/>
	          <Route path = "/register" element={<Register />}/>
	          <Route path = "/login" element={<Login />}/>
	          
	          <Route path = "*" element = {<Notfound />}/>
	        </Routes>
	    </Router>
	    </userContext.Provider>
		)
}