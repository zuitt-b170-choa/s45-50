import React, {useState, useEffect, useContext} from 'react';

import {Navigate} from 'react-router-dom'

import userContext from '../userContext.jsx'
import {Form, Container, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function Login(){

	const {user, setUser} = useContext(userContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";

		if(isEmailNotEmpty && isPasswordNotEmpty){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	},[email, password]);

	function login(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/users/login', {
			method: "POST",
			headers:{"Content-Type": "application/json"},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then((res)=>res.json())
		.then((res)=>{
			console.log(res.access)
			if(res.access !== undefined){
				Swal.fire("You are now logged in.");
				localStorage.setItem('access', res.access);
				setUser({access: res.access})
			}else{
				alert(res.error);
				setEmail("");
				setPassword("");
			}
		})
		//localStorage.setItem = to store a piece of data inside the localstorage of the browser, this is because the local storage does not delete unless it is manually done or the codes make it delete the information
		/*localStorage.setItem('email', email);
		setUser({email:email});*/
	}
	// navigate allows us to redirect the users after logging in and updating the global user state. even if the user tries to input the /login as URI, it would still redirect the user to the home page.
		//replace to attribute - to specify the page/uri to where the user/s will be redirected
		
	if(user.access !== null){
		return <Navigate replace to ="/"/>
	}

	return(
		<Container fluid>
			<h1>Login</h1>
			<Form onSubmit = {login}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type = "email" placeholder="Enter Email" value = {email} onChange={(e)=>setEmail(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type = "password" placeholder="Enter Password" value = {password} onChange={(e)=>setPassword(e.target.value)} required />
				</Form.Group>
				<Button variant="secondary" type="submit" disabled={isDisabled}>Login</Button>
			</Form>
		</Container>
		)
}