import React, {useState, useEffect, useContext} from 'react';
import {Form, Container, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import userContext from '../userContext.jsx'

export default function Register(){

	const navigate = useNavigate();
	const {user} = useContext(userContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [passwordConfirm, setPasswordConfirm] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isFirstNameNotEmpty = firstName !== "";
		let isLastNameNotEmpty = lastName !== "";
		let isAgeNotEmpty = age !== "";
		let isGenderNotEmpty = gender !== "";
		let isMobileNoNotEmpty = mobileNo !== "";

		let isEmailNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";
		let isPasswordConfirmNotEmpty = passwordConfirm !== "";
		let isPasswordMatch = password === passwordConfirm;

		if(isFirstNameNotEmpty && isLastNameNotEmpty && isAgeNotEmpty && isGenderNotEmpty && isMobileNoNotEmpty && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch && mobileNo.length === 11){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}

	},[firstName, lastName, age, gender, email, mobileNo, password, passwordConfirm]);

	/*
	Two_Way Binding
		to be able to capture or save the input value from the input elements, we can bind the values of the elements with the states. We, as devs, cannot type into the inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that is bound to the input.

		Two-way binding is done so that we can assure that we can save the inputs into our states as the users type into the element. This is so that we dont have to save it before submitting.
	*/

	function register(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/users/checkEmail',{
			method: "POST",
			headers:{"Content-Type": "application/json"},
			body: JSON.stringify({
				email: email
			})
		})
		.then((res)=>res.json())
		.then((res)=>{
			console.log(res)
			if(res === true){
				Swal.fire("Duplicate email. Use another email.");
				setEmail("");
			}else{
				fetch('http://localhost:4000/api/users/register', {
				method: "POST",
				headers:{"Content-Type": "application/json"},
				body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				age: age,
				gender: gender,
				email: email,
				password: password,
				mobileNo: mobileNo
					})
				})
			.then((res2)=>res2.json())
			.then((res2)=>{
				console.log(res2);
				Swal.fire("Registration Successful! Welcome to Zuitt");
				navigate('/login');
				})
			}		
		})	
}

	if(user.access !== null){
		return <Navigate replace to="/courses"/>
	}

	/*
		e.target.value 
		e - the event to which the element will listen
		target - the element where the event will happen
		value - the value that the user has entered in the element
	*/
	return(
		<Container>
			<Form onSubmit = {register}>
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type = "text" placeholder="Enter First Name" value = {firstName} onChange={(e)=>setFirstName(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type = "text" placeholder="Enter Last Name" value = {lastName} onChange={(e)=>setLastName(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Age</Form.Label>
					<Form.Control type = "number" placeholder="Enter Age" value = {age} onChange={(e)=>setAge(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Gender</Form.Label>
					<Form.Control type = "text" placeholder="Enter Gender" value = {gender} onChange={(e)=>setGender(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type = "email" placeholder="Enter Email" value = {email} onChange={(e)=>setEmail(e.target.value)} required/>
					<Form.Text className = "text-muted">We'll never share your email to anyone else.</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type = "number" minlength="11" placeholder="Enter Mobile Number" value = {mobileNo} onChange={(e)=>setMobileNo(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type = "password" placeholder="Enter Password" value = {password} onChange={(e)=>setPassword(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type = "password" placeholder="Enter Password" value = {passwordConfirm} onChange={(e)=>setPasswordConfirm(e.target.value)} required/>
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>
		)
}