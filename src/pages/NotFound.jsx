import	React from 'react';
import { Link } from 'react-router-dom';

// Bootstrap dependencies
import Container from 'react-bootstrap/Container';

// App Components
import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Notfound(){
	return (
			<div className = "NotFound">
				<h1>Page Not Found</h1>
				<p>Go back to the<Link to="/"> homepage.</Link></p>
			</div>
		)
}