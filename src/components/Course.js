// dependencies
import React, { useState, useEffect, setIsDisabled } from 'react';

// Bootstrap Components
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';



export default function Course(props) {

	/*
		props - short term for properties. similar to the arguments/paremeters found inside the functions. a way for the parent component to receive information.
			through the use props. devs can use the same coomponent and feed different information/data for rendering
	*/
	let course = props.course;
	/*
		useState() used in React to allow components to create manage its own data and is meant to be used internally
			- accepts an argument that is meant to be the value of the first element in array

		in react.js, state values must not be changed directly. all changes to the state values must be through the setState() function
			- setState() is the second element in the create Array

		the enrollees will start at zero. the result of the useState() is an array of data that is then destructured into count and setCount.

		setCount function is used to update the value of the count variable, depending on the times that the enroll function is triggered by the onClick command(button event)
	*/
	const [isDisabled, setIsDisabled] = useState(0)
	const [seat, seatCount] = useState(30)

	/*function enroll(){
		setCount(count + 1)
		console.log(count)
		seatCount(seat - 1)

		if(count === 30){
			alert("No more seats.");
			setCount(count);
			seatCount(seat);
		}
		if(seat === 0){
			alert("No more seats.")

		}else{
			setCount(count + 1);
			seatCount(seat - 1)
		}
	}*/

	useEffect(()=>{
		if(seat === 0){
			alert("No more seats available.")
			setIsDisabled(true)
		}
	},[seat])

	return (
		<Card className = "card-course">
			<Card.Body>
				<Card.Title>
					<h4>{course.name}</h4>
				</Card.Title>
				<Card.Text>
				<h6>Description</h6>
				<p>{course.description}</p>
				<h6>Price:</h6>
				PhP {course.price}<br></br><br></br>
				<h6>Enrollees</h6>
				<p>{seat} Remaining</p>
				<Button variant="primary" onClick = {()=>seatCount(seat - 1)} disabled = {isDisabled}>Enroll</Button>
				</Card.Text>
			</Card.Body>
		</Card>
		);

}