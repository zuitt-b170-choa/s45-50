import React, {useState, useEffect} from 'react';

import Container from 'react-bootstrap/Container'

export default function Counter(){
	const [count, setCount] = useState(0);
	const [ingredients, setIngredients] = useState(0);

	/*
		Effect Hook - allows us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes
			- the devs need useEffect for the page (or atleast part of the page) to be reactive
		useEffect - requires two argumentsl; function and the array
			- function - to specify the codes to be executed 
			- array - to set which variable is to be listened to, in terms of changing the state for the function to be executed. in some cases, not having any dependency array will cause the page to have loops.
	*/
	useEffect(()=>{
		document.title = `You clicked ${count} times`;
		// console.log(`You clicked ${count} times`)
		/*if(count >= 0){
			console.log(`You clicked ${count} times`)
		}*/
	},[])

	return(
		<Container>
			<p>You clicked {count} times.</p>
			<button onClick = {()=> setCount(count+1)}>Click me!</button>
		</Container>
	)
}