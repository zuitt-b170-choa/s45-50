export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce purus metus, faucibus in varius vitae, tincidunt et nulla. Integer a mattis ipsum, sit amet lobortis quam. Ut porttitor diam metus, ac ornare libero malesuada in. Donec et odio malesuada, accumsan leo ullamcorper, convallis massa. Proin diam augue, sagittis ac risus et, porta hendrerit metus. Suspendisse est odio, egestas nec viverra vitae, maximus eget arcu. Ut at leo non ante ultrices ultricies in sit amet urna. Curabitur urna lacus, vulputate et venenatis eu, mattis vitae ipsum. Quisque interdum iaculis orci, quis consequat orci molestie ut.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce purus metus, faucibus in varius vitae, tincidunt et nulla. Integer a mattis ipsum, sit amet lobortis quam. Ut porttitor diam metus, ac ornare libero malesuada in. Donec et odio malesuada, accumsan leo ullamcorper, convallis massa. Proin diam augue, sagittis ac risus et, porta hendrerit metus. Suspendisse est odio, egestas nec viverra vitae, maximus eget arcu. Ut at leo non ante ultrices ultricies in sit amet urna. Curabitur urna lacus, vulputate et venenatis eu, mattis vitae ipsum. Quisque interdum iaculis orci, quis consequat orci molestie ut.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce purus metus, faucibus in varius vitae, tincidunt et nulla. Integer a mattis ipsum, sit amet lobortis quam. Ut porttitor diam metus, ac ornare libero malesuada in. Donec et odio malesuada, accumsan leo ullamcorper, convallis massa. Proin diam augue, sagittis ac risus et, porta hendrerit metus. Suspendisse est odio, egestas nec viverra vitae, maximus eget arcu. Ut at leo non ante ultrices ultricies in sit amet urna. Curabitur urna lacus, vulputate et venenatis eu, mattis vitae ipsum. Quisque interdum iaculis orci, quis consequat orci molestie ut.",
		price: 55000,
		onOffer: true
	}
	
]