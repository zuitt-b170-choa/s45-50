import React from 'react';

/*
	React.createContext()
		= create a Context Object
			- a special object that allows information storage within the app and pass it around the components
			- with this, we'd be able to create global state to store infomration/user details instead of getting from local storage from time to time.
			- a different approach to passing information between components without the use of props and pass it from parents to child since it is already being passed on to other components as a global state for the user
*/
export default React.createContext();